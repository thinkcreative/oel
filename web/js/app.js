+function($) {

App = Ember.Application.create();

$(window).on('hashchange',function(){
    window.location.reload(true);
});

var query = [];
/*
 * Ember Router Map
 * 
 * Routes defined for application
 *
 * This is where the url paths are determined for the application,, they work off the main index page of the application
 * followed by #/{url path}. The main page is predefined as index and is only route not specified here.
 */
App.Router.map(function(){
    this.resource('coalition', { path: '/coalition/:id' });
    this.resource('county', { path: '/county/:id' });
    this.resource('countycoalition', { path: '/county/coalition/:county/:coalition' });
    this.resource('terms');
});

/*
 * Ember Routes
 * With each url path there is a specific call made to the api to pull in the correct data. The
 * data pulled in is then processed through the appropriate functions so that the divs can be
 * generated properly.
 */

App.ApplicationRoute = Ember.Route.extend({
   renderTemplate: function() {
        this.render();
        this.render('breadcrumbs', {
            outlet: 'breadcrumbs',
            into: 'application',
            controller: this.controllerFor('breadcrumbs')
        });
    }
});

App.IndexRoute = Ember.Route.extend({
    model: function() {
        query = apiCall('//www.earlylearningdashboarddata.com/oelapi/index');
        chart_array = chartDivs(query, 'index');
        return chart_array;
    }
});

App.CoalitionRoute = Ember.Route.extend({
    model: function(params) {
        query = apiCall("//www.earlylearningdashboarddata.com/oelapi/coalition/"+params.id);
        var county_array = imagemap(query);
        return county_array;
    }
});

App.CountyRoute = Ember.Route.extend({
    model: function(params) {
        var chart_array = [];
        query = apiCall("//www.earlylearningdashboarddata.com/oelapi/county/"+params.id);
        chart_array = chartDivs(query, 'county');
        return chart_array;
    }
});

App.CountycoalitionRoute = Ember.Route.extend({
    model: function(params) {
        var chart_array = [],
            paragraph = '';
        query = apiCall("//www.earlylearningdashboarddata.com/oelapi/county/coalition/"+params.county+"/"+params.coalition);
        if(params.county != 'ALL'){
            paragraph = 'county';
        }else{
            paragraph = 'coalition';
        }
        chart_array = chartDivs(query, paragraph);
        return chart_array;
    }
});

/*
 * Ember Views
 *
 * These are used to call functions after html has been rendered to the
 * page. This allows the google charts to only be applied after all
 * the necessary html is in place. Each one supports it's proper view
 * and following the google chart render sets up tabs and slides for
 * the page.
 */
App.IndexView = Ember.View.extend({
    templateName:"index",
    drawChart: function(){
        renderCharts(query);
        tabs();
        slide();
    }.on("didInsertElement")
});

App.CountycoalitionView = Ember.View.extend({
    templateName:"countycoalition",
    drawChart: function(){
        renderCharts(query);
        tabs();
        slide();
    }.on("didInsertElement")
});

App.CountyView = Ember.View.extend({
    templateName:"county",
    drawChart: function(){
        renderCharts(query);
        tabs();
        slide();
    }.on("didInsertElement")
});

/*
 * Ember Controllers
 *
 * Controllers allow templates to be reused for more
 * than one route. These controllers render the drow downs
 * and county list for various coalitions at the bottom
 * of the page.
 */

App.ApplicationController = Ember.Controller.extend({
	needs: ['breadcrumbs'],
	currentPathDidChange: function() {
	    var breadcrumb = [], context = [];
        path = this.get('currentPath');
		pathdata = this.get('target.router.currentHandlerInfos');
        if(path == 'terms'){
            context = {'titlelink': "/", 'title': "Early Learning Dashboard", 'withdash': false};
            breadcrumb.push({'context': context});
            context = {'titlelink': false, 'title': "Terms", 'withdash': true};
            breadcrumb.push({'context': context});
            pathdata = breadcrumb;
        }else if (pathdata[1].context.title == "Early Learning Dashboard - Statewide") {
			pathdata[1].context.title = "Early Learning Dashboard";
		} else {
		    var title = pathdata[1].context.title.split(" - ");
			pathdata[0].context = {'titlelink' : "/", 'title' : "Early Learning Dashboard"};
			if(typeof pathdata[1].context.coalition_id == 'undefined'){
                pathdata[1].context = { 'titlelink': false, 'title': title[0], 'withdash': true };
            }else{
                pathdata[1].context = { 'titlelink': "#/coalition/"+pathdata[1].context.coalition_id, 'title': title[0], 'withdash': true };
            }
			context = { 'titlelink': "", 'title': title[1], 'withdash': true };
			pathdata.push({'context': context});
		}
        this.get('controllers.breadcrumbs').set('content', pathdata);
    }.observes('currentPath')
});

App.BreadcrumbsController = Em.ArrayController.extend({});

App.CountydropdownController = Ember.ObjectController.extend({
    county: function(){
        var countydropdown = apiCall('//www.earlylearningdashboarddata.com/oelapi/countydropdown');
        return countydropdown;
    }.property('model.countydropdown')
});

App.CoalitiondropdownController = Ember.ObjectController.extend({
    coalition: function(){
        var coalitiondropdown = apiCall('//www.earlylearningdashboarddata.com/oelapi/coalitiondropdown');
        return coalitiondropdown;
    }.property('model.coalitiondropdown')
});

App.CountycoalitionhybridController = Ember.ObjectController.extend({
    countycoalitionhybrid: function(params){
        var model = this.get('model'),
            coalitionview = apiCall("//www.earlylearningdashboarddata.com/oelapi/coalition/"+model.coalition_id),
            countycoalitionhybrid = imagemap(coalitionview);
        countycoalitionhybrid['title'] = coalitionview.title;
        return countycoalitionhybrid;
    }.property('model.countycoalitionhybrid')
});

/*
 * Ember Action Views
 *
 * These are views that are set to respond to a specific action
 * on the page. When browsing through a route or clicking one
 * of the dropdowns it appropriately pulls the data and reloads
 * the page.
 */
App.ClickableView = Ember.View.extend({
    click: function(evt) {
        location.href = evt.target.getAttribute('data-link');
    }
});

App.DropdownView = Ember.View.extend({
    click: function(evt) {
        var target = evt.target.getAttribute('data-ul'),
            split = target.split(' '),
            arrow = $(split[0]+' .widget-display');
        arrow.toggleClass('drop');
        $(target).toggleClass('hide');
    }
});

/*
 * apicall() function
 *
 * Function that is called to pull the json string from the api.
 */
function apiCall(url){
    var result = "";
    $.ajax(
        {
            "url": url,
            "async": false,
            "type": "get",
            "dataType": "json",
            "success": function(data) {
                result = data;
            }
        }
    );
    return result;
}

/*
 * chartDivs() function
 *
 * This function is necessary to generate the html to the page
 * for the google charts to be displayed in. Because this function is used for many
 * different scenarios that were expressed to as later in the project it has
 * become quite large. This should be optimized at a later date if possible.
 */
function chartDivs(data, type){
    var tempArr = [],
        divArr = [],
        subArr = [],
        subArray = '',
        description = '',
        count = 0;

    for (var key in data.charts) {
        if('description' in data.charts[key]){
            for (var subkey in data.charts[key]) {
                if(typeof data.charts[key][subkey] === 'object' && subkey != 'tabs' && typeof data.charts[key]['tabs'] != 'undefined' && data.charts[key][subkey]['type'] != 'summary'){
                    subArr.push({'name': 'chart-'+subkey, 'chart_title': data.charts[key]['tabs'][count]});
                    count += 1;
                }else if(typeof data.charts[key][subkey] === 'object' && subkey != 'tabs' && data.charts[key][subkey]['type'] != 'summary'){
                    subArr.push({'name': 'chart-'+subkey, 'chart_title': data.charts[key][subkey]['chart_title']});
                }
            }
            if(subArr.length > 1){
                subArray = true;
            }else{
                subArray = false;
            }
            if(typeof data.charts[key]['summary_total'] != 'undefined'){
                description = data.charts[key]['description'].replace('[Number/TOTAL from DB]',commaSeparateNumber(data.charts[key]['summary_total']));
            }else{
                description = data.charts[key]['description'].replace('[Number/TOTAL from DB]','0');
            }
            if(type == 'index'){
                description = description.replace('[statewide/in this coalition/in this county]', 'statewide');
            }else if(type == 'coalition'){
                description = description.replace('[statewide/in this coalition/in this county]', 'in this coalition');
            }else{
                description = description.replace('[statewide/in this coalition/in this county]', 'in this county');
            }
            divArr.push({'subarray': subArray, 'chart_title': data.charts[key]['chart_title'], 'description': description, 'subdiv': subArr});
            subArr = [], subArray = false, description = '', count = 0;
        }
        if('category_id' in data.charts[key]){
            divArr.push({'name': 'chart-'+data.charts[key]['category_id'], 'chart_title': data.charts[key]['chart_title'], 'subarray': false});
        }
        if('item_number' in data.charts[key]){
            divArr.push({'name': 'chart-'+data.charts[key]['item_number'], 'chart_title': data.charts[key]['chart_title'], 'subarray': false});
        }
    }

    tempArr = {'title': data.title, 'date': data.date, 'coalition': data.coalition, 'county': data.county, 'div': divArr, 'coalition_id': data.coalition_id};
    return tempArr;
}

/*
 * renderCharts() function
 *
 * This function is called after all the html is generated. It will
 * loop over the chart data and do a call to drawChart to display
 * each google chart to the page.
 */
function renderCharts(data){
    for (var key in data.charts) {
        if('description' in data.charts[key]){
            for (var subkey in data.charts[key]) {
                if(typeof data.charts[key][subkey] == 'object' && subkey != 'tabs'){
                    drawChart(data.charts[key][subkey]);
                }
            }
        }
        if('category_id' in data.charts[key] || 'item_number' in data.charts[key]){
            drawChart(data.charts[key]);
        }
    }
}

/*
 * drawChart() Function
 * 
 * The one function that takes the chart data and generates a single
 * google chart to the page. It is looped over multiple times to
 * generate all the charts through the renderCharts function.
 */
function drawChart(data) {
    if(data['type'] != 'summary'){
        var dataChart  = new google.visualization.DataTable(
        {
            cols: data['cols'],
            rows: data['rows']
        });
        var options = {
            title: data['chart_title'],
            interpolateNulls: true,
            sliceVisibilityThreshold: 0
        };

        var formatchart = (data.chart_title.indexOf('Payment') != -1) ? new google.visualization.NumberFormat({negativeParens: true, prefix: '$'}) : new google.visualization.NumberFormat({negativeParens: true, pattern: '###,###.##'});
        formatchart.format(dataChart, 1);
        if(data['cols'].length > 2){
            formatchart.format(dataChart, 2);
        }
        if(data['type'] == 'line'){
            var chart = new google.visualization.LineChart(document.getElementById('chart-'+data['item_number']));
			if (data.chart_title.indexOf('Payment') != -1) options.vAxis = {format: '$#,###'};
        }else{
            var chart = new google.visualization.PieChart(document.getElementById('chart-'+data['category_id']));
        }
        chart.draw(dataChart, options);
    }
}

/*
 * imagemap() function
 * 
 * For coalition views and for counties to be displayed
 * for a coalition in a county view this is used to map
 * all the images.
 */
function imagemap(data){
    var map = {
        "1": "Alachua.jpg",
        "2": "Baker_tn.jpg",
        "10": "Clay_tn.jpg",
        "19": "Franklin_tn.jpg",
        "27": "HernandoCourthouse_tn.jpg",
        "35": "Lake-CrookedBird_tn.jpg",
        "43": "Martin_tn.jpg",
        "50": "Palm Beach_tn.jpg",
        "55": "St. Johns_tn.jpg",
        "66": "Walton_tn.jpg",
        "3": "Bay_tn.jpg",
        "11": "Collier_tn.jpg",
        "20": "Gadsden_tn.jpg",
        "28": "Highlands_tn.jpg",
        "36": "Lee_tn.jpg",
        "13": "Miami-Dade-Skyline_tn.jpg",
        "51": "Pasco_tn.jpg",
        "56": "St. Lucie_tn.jpg",
        "67": "Washington_tn.jpg",
        "4": "Bradford_tn.jpg",
        "12": "Columbia_tn.jpg",
        "21": "Gilchrist_tn.jpg",
        "29": "Hillsborough_tn.jpg",
        "37": "Leon_tn.jpg",
        "44": "Monroe-SouthernmostBuoy_tn.jpg",
        "52": "Pinellas_tn.jpg",
        "60": "Sumter_tn.jpg",
        "5": "Brevard-ShuttleLaunch_tn.jpg",
        "14": "Desoto_tn.jpg",
        "22": "Glades_tn.jpg",
        "30": "Holmes_tn.jpg",
        "38": "Levy_tn.jpg",
        "45": "Nassau_tn.jpg",
        "53": "Polk_tn.jpg",
        "61": "Suwannee_tn.jpg",
        "6": "Broward_tn.jpg",
        "15": "Dixie_tn.jpg",
        "23": "Gulf_tn.jpg",
        "31": "Indian River_tn.jpg",
        "39": "Liberty_tn.jpg",
        "46": "Okaloosa_tn.jpg",
        "54": "Putnam_tn.jpg",
        "62": "Taylor_tn.jpg",
        "7": "Calhoun_tn.jpg",
        "16": "Duval_tn.jpg",
        "24": "Hamilton_tn.jpg",
        "32": "Jackson_tn.jpg",
        "40": "Madison_tn.jpg",
        "47": "Okeechobee_tn.jpg",
        "57": "Santa Rosa_tn.jpg",
        "63": "Union_tn.jpg",
        "8": "Charlotte_tn.jpg",
        "17": "Escambia_tn.jpg",
        "25": "Hardee_tn.jpg",
        "33": "Jefferson_tn.jpg",
        "41": "Manatee_tn.jpg",
        "48": "Orange-OrlandoHistorical_tn.jpg",
        "58": "Sarasota_tn.jpg",
        "64": "Volusia_tn.jpg",
        "9": "Citrus_tn.jpg",
        "18": "Flagler_tn.jpg",
        "26": "Hendry_tn.jpg",
        "34": "Lafayette_tn.jpg",
        "42": "Marion-HorseCountry_tn.jpg",
        "49": "Osceola_tn.jpg",
        "59": "Seminole_tn.jpg",
        "65": "Wakulla_tn.jpg"
    };
    for (var key in data['div']) {
        if(data['div'].hasOwnProperty(key)) {
            data['div'][key]['image'] = '/images/thumbnails/'+map[data['div'][key].county_id];
        }
    }
    return data;
}

/*
 * commaSeparateNumber() function
 *
 * Simple function to add comma's for summary values
 * in paragraphs.
 */
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
}

/*
 * tabs() function
 *
 * Custom tabs for the charts
 */
function tabs(){
    $(window).ready(function(){
        $('.tabs').each(function(){
            if($(this).find('ul').length > 0){
                $(this).find('ul li:first-child').addClass('current');
                $(this).find('> div').hide();
                $($(this).find('ul li:first-child').find('span').html()).show();
            }
            $(this).on('click', 'li', function(){
                var li = $(this),
                    content = li.find('span').html();
                li.parent().find('li.current').removeClass('current');
                $(this).addClass('current');
                li.parent().parent().find('> div').hide();
                $(content).show();
            });
        });
    });
}

/*
 * slide() function
 *
 * Custom slider for the charts
 */
function slide(){
    $(window).ready(function(){
        $('.chart-container').each(function(){
            var hidediv = $(this).find('> div:not(.description)');
            hidediv.hide();
            $(this).on('click', 'a.display-chart', function(){
                var span = $(this).find('span'),
                    div = $(this).siblings('div:not(.description)');
                if(span.html() == 'View'){
                    span.text('Close');
                }else{
                    span.text('View');
                }
                div.slideToggle();
            });
        });
    });
}

}(jQuery);