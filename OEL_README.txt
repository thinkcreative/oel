==System==

The symfony stack was used to build out the php for database calls and url redirecting. Most files can be ignored in converting it to a ".net" system. Only these files need to be considered directly when converting:

-app/config/config.yml
Configuration file for database settings

-app/config/routing.yml
Settings for urls to point to various functions in OELApiController.php

-src/Project/SiteBundle/Controller/OELApiController.php
This file is where most of the work happens, it makes the database calls and processes the data to be sent back as a json string to the browser. Each path correlates to a specific function in this file, thus urls will be passed to the following functions:

Home Page (Index):
url: /oelapi/index
function: IndexAction

Coalition View:
url: /oelapi/coalition/{id}
function: CoalitionAction

County View:
url: /oelapi/county/{county}
function:  CountyAction

County Coalition View:
url: /oelapi/county/coalition/{county}/{id}
function: CountyCoalitionAction

County Dropdown:
url: /oelapi/countydropdown
function: CountyDropdownAction

Coalition Dropdown:
url: /oelapi/coalitiondropdown
function: CoalitionDropdownAction


-src/Project/SiteBundle/Command/ImportDataCommand.php
For importing data from the spreadsheets into the database. It will dump the entire table and then upload the csv to update that specific table. It updates the tables sample_data, coalition_data, chart_mapping, and item_numbers. The sample_data and coalition_data should be the only tables needing updates for the site each month.

-web/js/app.js
Where all javascript is performed to have the application function.

==Library files for javascript==
-web/js/ember-1.9.1.js
-web/js/handlebars-v2.0.0.js
-web/js/jquery-1.10.2.js
-web/js/json3.min.js
-web/js/handlebars-v2.0.0.js

==HTML Page==
-web/bundles/charts.html

==Directories to copy==
-web/js
-web/images
-web/css

==MySQL table break down==

=sample_data=
Where the actual data is recorded and associates the proper coalition, counties, and time of recording with other tables.

data_thru - date data was collected
fiscal_year - determines if previous or current fiscal year
item_number - group data is for
month - month of year data was taken
coalition_id - coalition the data belongs to
coun_c - county id the data belongs to
result - actual data recorded
run_date - date data was pulled from database


=coalition_data=
Associates the county with a coalition and organizes the order of coalitions for display in drop down.

coun_c - county id
coun_c_name - county name
coalition_id - coalition id
coalition_name - coalition name
dbid - database id
coalition_order - order which coalitions to be displayed in drop down


=item_numbers=
The group name, description, and category for pie charts.

item - item id
item_description - description of item
notes - notes of item
category_id - pie chart category the item belongs to


=county=
Maps county ID to a county name

coun_c - county ID
coun_c_name - county name


=coalition=
Maps coalition ID to coalition name

coalition_id - coalition ID
coalition_name - coalition name


=chart_mapping=
chart_id - main chart set to display in
summary_indicator - title for set of charts
description - summary to be shown for set of charts
pie_chart - first pie chart name
line_chart - name of first line chart
pie_chart_code - first pie chart id
line_chart_code - second line chart id
total_code - data to pull total number for summary
second_pie_chart_code - second pie chart id
first_tab - first tab title
second_tab - second tab title
third_tab - third tab title

