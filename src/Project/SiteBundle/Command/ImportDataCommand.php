<?php

namespace Project\SiteBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
//use Doctrine\DBAL\Connection;
use Doctrine\ORM\Tools\EntityGenerator;
//use Symfony\Component\HttpFoundation\File\File;

class ImportDataCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('site:import')
            ->setDescription('Hello World example command')
            ->addArgument('file', InputArgument::OPTIONAL, 'File to Import', 'file')
            ->addArgument('table', InputArgument::OPTIONAL, 'Table to Import to', 'table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $databaseTable = $input->getArgument('table');
        $file = fopen($input->getArgument('file'), 'r');
        $drop = "DROP TABLE $databaseTable";
        $doctrineManager= $this->getContainer()->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();
        $dropTable = $em->prepare($drop);
        $dropTable->execute();
        if($databaseTable == 'sample_data'){
            $createSample = "CREATE TABLE IF NOT EXISTS `sample_data` (`data_thru` int(11) NOT NULL, `fiscal_year` varchar(255) NOT NULL, `item_number` varchar(255) NOT NULL, `month` int(11) NOT NULL, `coalition_id` varchar(255) NOT NULL, `coun_c` varchar(255) NOT NULL, `result` double NOT NULL, `run_date` int(11) NOT NULL) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8;";
        }elseif($databaseTable == 'coalition_data'){
            $createSample = "CREATE TABLE IF NOT EXISTS `coalition_data` (`coun_c` int(11) NOT NULL, `coun_c_name` varchar(255) NOT NULL, `coalition_id` int(11) NOT NULL, `coalition_name` varchar(255) NOT NULL, `dbid` int(11) NOT NULL, `coalition_order` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        }elseif($databaseTable == 'chart_mapping'){
            $createSample = "CREATE TABLE IF NOT EXISTS `chart_mapping` ( `chart_id` int(11) NOT NULL, `summary_indicator` varchar(255) NOT NULL, `description` varchar(500) NOT NULL, `pie_chart` varchar(255) DEFAULT NULL, `line_chart` varchar(255) DEFAULT NULL, `pie_chart_code` int(11) DEFAULT NULL, `line_chart_code` varchar(255) DEFAULT NULL, `total_code` varchar(255) DEFAULT NULL, `second_pie_chart_code` int(11) DEFAULT NULL, `first_tab` varchar(255) DEFAULT NULL, `second_tab` varchar(255) DEFAULT NULL, `third_tab` varchar(255) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        }elseif($databaseTable == 'item_numbers'){
            $createSample = "CREATE TABLE IF NOT EXISTS `item_numbers` ( `item` varchar(255) NOT NULL, `item_description` varchar(255) NOT NULL, `notes` varchar(255) NOT NULL, `category_id` int(11) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        }
        $sampleTable = $em->prepare($createSample);
        $sampleTable->execute();


        if($file !== FALSE) {

            // define import data column/field headers
            foreach($Headers = fgetcsv($file) as $Key => $Name) {
                if($Name != ""){
                    $Headers[$Key] = $Name;
                }
            }

            $DataRowCount = 0;
            while(($DataRow = fgetcsv($file)) !== FALSE) {
                // skip empty data rows lines
                if(count($DataRow) && $DataRow[0] !== NULL) {
                    $DataRow = array_combine($Headers, $DataRow);

                    if($databaseTable == 'sample_data'){
                        $DataRow['run_date'] = strtotime($DataRow['run_date']);
                        $DataRow['data_thru'] = strtotime($DataRow['data_thru']);
                        $DataRow['month'] = strtotime($DataRow['month']);
                        $em->insert($databaseTable,
                                array(
                                    'data_thru' => $DataRow['data_thru'],
                                    'fiscal_year' => $DataRow['fiscal_year'],
                                    'item_number' => $DataRow['item_number'],
                                    'month' => $DataRow['month'],
                                    'coalition_id' => $DataRow['coalition_id'],
                                    'coun_c' => $DataRow['coun_c'],
                                    'result' => $DataRow['result'],
                                    'run_date' => $DataRow['run_date']
                                )
                        );
                    }elseif($databaseTable == 'coalition_data'){
                        $em->insert($databaseTable,
                                array(
                                    'coun_c' => $DataRow['coun_c'],
                                    'coun_c_name' => $DataRow['coun_c_name'],
                                    'coalition_id' => $DataRow['coalition_id'],
                                    'coalition_name' => $DataRow['coalition_name'],
                                    'dbid' => $DataRow['dbid'],
                                    'coalition_order' => $DataRow['coalition_order']
                                )
                        );
                    }elseif($databaseTable == 'chart_mapping'){
                        $em->insert($databaseTable,
                                array(
                                    'chart_id' => $DataRow['chart_id'],
                                    'summary_indicator' => $DataRow['summary_indicator'],
                                    'description' => $DataRow['description'],
                                    'pie_chart' => $DataRow['pie_chart'],
                                    'line_chart' => $DataRow['line_chart'],
                                    'pie_chart_code' => $DataRow['pie_chart_code'],
                                    'line_chart_code' => $DataRow['line_chart_code'],
                                    'total_code' => $DataRow['total_code'],
                                    'second_pie_chart_code' => $DataRow['second_pie_chart_code'],
                                    'first_tab' => $DataRow['first_tab'],
                                    'second_tab' => $DataRow['second_tab'],
                                    'third_tab' => $DataRow['third_tab']
                                )
                        );
                    }elseif($databaseTable == 'item_numbers'){
                        $em->insert($databaseTable,
                                array(
                                    'item' => $DataRow['item'],
                                    'item_description' => $DataRow['item_description'],
                                    'notes' => $DataRow['notes'],
                                    'category_id' => $DataRow['category_id']
                                )
                        );
                    }
                    $DataRowCount++;
                }
            }
            echo "Inserted $DataRowCount Records\n";
        }
    }
}