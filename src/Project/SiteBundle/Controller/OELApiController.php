<?php

namespace Project\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\EntityGenerator;

class OELApiController extends Controller
{

    /*
     * IndexAction() function
     *
     * This generates the data for the dashboard home with pulls all the state data.
     */
    public function IndexAction()
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();

        //Returns Fiscal Year Unix Time Stamps
        $years = $this->fiscalyears();

        //Previous Fiscal Year Line Chart SQL
        $prevfiscalSQL="    SELECT DISTINCT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, sample_data.month, sample_data.result, sample_data.data_thru
                            FROM sample_data
                            INNER JOIN item_numbers
                            ON sample_data.item_number = item_numbers.item
                            WHERE sample_data.coun_c = 'STATE'
                            AND sample_data.coalition_id = 'STATE'
                            AND item_numbers.category_id = 0
                            AND (sample_data.month BETWEEN $years[0] AND $years[1])
                            ORDER BY sample_data.month ASC;";
        $statement = $em->prepare($prevfiscalSQL);
        $statement->execute();
        $PrevQuery = $statement->fetchAll();

        //Current Fiscal Line Chart SQL
        $currentfiscalSQL=" SELECT DISTINCT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, sample_data.month, sample_data.result, sample_data.data_thru
                            FROM sample_data
                            INNER JOIN item_numbers
                            ON sample_data.item_number = item_numbers.item
                            WHERE sample_data.coun_c = 'STATE'
                            AND sample_data.coalition_id = 'STATE'
                            AND item_numbers.category_id = 0
                            AND (sample_data.month > $years[1])
                            ORDER BY sample_data.month ASC;";
                        
        $statement = $em->prepare($currentfiscalSQL);
        $statement->execute();
        $CurrentQuery = $statement->fetchAll();

        //Determines through date to be presented in on page
        $date = (isset($CurrentQuery[0])) ? date('F j, Y', $CurrentQuery[0]['data_thru']) : date('F j, Y', $PrevQuery[0]['data_thru']);

        //Processes the previous and current fiscal year sql queries into presentable charts
        $LineResult = $this->linechart($PrevQuery, $CurrentQuery, $years);

        //Pie Chart SQL
        $pieSQL="   SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, category.description, sample_data.month, sample_data.result
                    FROM sample_data
                    INNER JOIN item_numbers
                    ON sample_data.item_number = item_numbers.item
                    INNER JOIN category
                    ON item_numbers.category_id = category.id
                    WHERE sample_data.coun_c = 'STATE'
                    AND sample_data.coalition_id = 'STATE'
                    AND item_numbers.category_id != 0
                    ORDER BY category.id ASC;";
        $statement = $em->prepare($pieSQL);
        $statement->execute();
        $PieQuery = $statement->fetchAll();

        //Process pie chart sql into presentable charts
        $PieResult = $this->piechart($PieQuery);

        //Combines the line and pie charts into the approriate arrays for display
        $charts = $this->chartsort($LineResult, $PieResult);

        $results = array('title' => 'Early Learning Dashboard - Statewide', 'date' => $date, 'charts' => $charts);
        $response = new Response(json_encode($results));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /*
     * CoalitionAction() function
     *
     * Pulls data for all counties in a coalition
     */
    public function CoalitionAction($id)
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();
        $coalitionSQL=" SELECT coalition.coalition_name
                        FROM coalition
                        WHERE coalition.coalition_id = $id
                        LIMIT 1;";
        $statement = $em->prepare($coalitionSQL);
        $statement->execute();
        $coalitionName = $statement->fetchAll();
        if($id != 33){
            //Will pull any coalition except RMCA
            $sql="  SELECT DISTINCT county.coun_c_name, county.coun_c, coalition.coalition_name, coalition.coalition_id
                    FROM coalition_data
                    INNER JOIN county
                    ON coalition_data.coun_c = county.coun_c
                    INNER JOIN coalition
                    ON coalition_data.coalition_id = coalition.coalition_id
                    WHERE coalition_data.coalition_id = $id;";
        }else{
            //If is RMCA County which changes often it will get all RMCA counties based on sample_data table
            $sql="  SELECT DISTINCT county.coun_c_name, county.coun_c, coalition.coalition_name, coalition.coalition_id
                    FROM sample_data
                    INNER JOIN county
                    ON sample_data.coun_c = county.coun_c
                    INNER JOIN coalition
                    ON sample_data.coalition_id = coalition.coalition_id
                    WHERE sample_data.coalition_id = 33
                    AND sample_data.coun_c != 0
                    ORDER BY county.coun_c_name ASC;";
        }
        $statement = $em->prepare($sql);
        $statement->execute();
        $counties = $statement->fetchAll();
        $item_array = array();
        foreach($counties as $item) {
            $item_array[] = array('county_name' => $item['coun_c_name'], 'county_id' => $item['coun_c'], 'coalition_name' => $item['coalition_name'], 'coalition_id' => $item['coalition_id'], 'link' => '#/county/coalition/'.$item['coun_c'].'/'.$item['coalition_id']);
        }
        $results = array('title' => $coalitionName[0]['coalition_name'], 'link' => '#/county/coalition/ALL/'.$id, 'div' => $item_array);
        $response = new Response(json_encode($results));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /*
     * CountyCoalitionAction() function
     *
     * When viewing a county with a specific coalition, this will only
     * pull date from that county within the specified coalition. This
     * is useful for counties that are both in RMCA and another coalition.
     */
    public function CountyCoalitionAction($county, $id)
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();
        $county = ($county == 'ALL' ? '"ALL"' : $county);
        $coalitionSQL=" SELECT coalition.coalition_name
                        FROM coalition
                        WHERE coalition.coalition_id = $id
                        LIMIT 1;";
        $statement = $em->prepare($coalitionSQL);
        $statement->execute();
        $coalitionName = $statement->fetchAll();
        $countySQL="    SELECT county.coun_c_name
                        FROM county
                        WHERE county.coun_c = $county
                        LIMIT 1;";
        $statement = $em->prepare($countySQL);
        $statement->execute();
        $countyName = $statement->fetchAll();

        //Returns Fiscal Year Unix Time Stamps
        $years = $this->fiscalyears();

        //Previous Fiscal Year Line Chart SQL
        $prevfiscalSQL="SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, sample_data.month, sample_data.result, sample_data.data_thru
                        FROM sample_data
                        INNER JOIN item_numbers
                        ON sample_data.item_number = item_numbers.item
                        WHERE sample_data.coun_c = $county
                        AND sample_data.coalition_id = $id
                        AND item_numbers.category_id = 0
                        AND (sample_data.month BETWEEN $years[0] AND $years[1])
                        ORDER BY sample_data.month ASC;";
        $statement = $em->prepare($prevfiscalSQL);
        $statement->execute();
        $PrevQuery = $statement->fetchAll();

        //Current Fiscal Line Chart SQL
        $currentfiscalSQL=" SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, sample_data.month, sample_data.result, sample_data.data_thru
                            FROM sample_data
                            INNER JOIN item_numbers
                            ON sample_data.item_number = item_numbers.item
                            WHERE sample_data.coun_c = $county
                            AND sample_data.coalition_id = $id
                            AND item_numbers.category_id = 0
                            AND (sample_data.month > $years[1])
                            ORDER BY sample_data.month ASC;";
                            
        $statement = $em->prepare($currentfiscalSQL);
        $statement->execute();
        $CurrentQuery = $statement->fetchAll();

        //Returns Fiscal Year Unix Time Stamps
        $date = (isset($CurrentQuery[0]['data_thru'])) ? date('F j, Y', $CurrentQuery[0]['data_thru']) : date('F j, Y', $PrevQuery[0]['data_thru']);

        //Processes the previous and current fiscal year sql queries into presentable charts
        $LineResult = $this->linechart($PrevQuery, $CurrentQuery, $years);

        //Pie Chart SQL
        $pieSQL="   SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, category.description, sample_data.month, sample_data.result
                    FROM sample_data
                    INNER JOIN item_numbers
                    ON sample_data.item_number = item_numbers.item
                    INNER JOIN category
                    ON item_numbers.category_id = category.id
                    WHERE sample_data.coun_c = $county
                    AND sample_data.coalition_id = $id
                    AND item_numbers.category_id != 0
                    ORDER BY category.id ASC;";
        $statement = $em->prepare($pieSQL);
        $statement->execute();
        $PieQuery = $statement->fetchAll();

        //Process pie chart sql into presentable charts
        $PieResult = $this->piechart($PieQuery);

        //Combines the line and pie charts into the approriate arrays for display
        $charts = $this->chartsort($LineResult, $PieResult);
        if(
            $id == 33
        ){
            $results = array('title' => $coalitionName[0]['coalition_name'].' - '.$countyName[0]['coun_c_name'], 'date' => $date, 'coalition' => $coalitionName[0]['coalition_name'], 'county' => $countyName[0]['coun_c_name'].' - RMCA', 'charts' => $charts, 'coalition_id' => $id);
        }elseif($county != '"ALL"'){
            $results = array('title' => $coalitionName[0]['coalition_name'].' - '.$countyName[0]['coun_c_name'], 'date' => $date, 'coalition' => $coalitionName[0]['coalition_name'], 'county' => $countyName[0]['coun_c_name'], 'charts' => $charts, 'coalition_id' => $id);
        }else{
            $results = array('title' => $coalitionName[0]['coalition_name'], 'date' => $date, 'coalition' => $coalitionName[0]['coalition_name'], 'charts' => $charts, 'coalition_id' => $id);
        }
        $response = new Response(json_encode($results));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /*
     * CountyAction() function
     *
     * Pull data for a specified county only despite
     * the coalition.
     */
    public function CountyAction($county)
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();

        //County SQL
        $countySQL="    SELECT county.coun_c_name, coalition_data.coalition_id
                        FROM county
                        INNER JOIN coalition_data
                        ON county.coun_c = coalition_data.coun_c
                        WHERE county.coun_c = $county
                        LIMIT 1;";
        $statement = $em->prepare($countySQL);
        $statement->execute();
        $countyName = $statement->fetchAll();

        //Returns Fiscal Year Unix Time Stamps
        $years = $this->fiscalyears();

        //Previous Fiscal Year Line Chart SQL
        $prevfiscalSQL="    SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, sample_data.month, sample_data.result, sample_data.data_thru, sample_data.coalition_id
                            FROM sample_data
                            INNER JOIN item_numbers
                            ON sample_data.item_number = item_numbers.item
                            WHERE sample_data.coun_c = $county
                            AND item_numbers.category_id = 0
                            AND (sample_data.month BETWEEN $years[0] AND $years[1])
                            ORDER BY sample_data.month ASC;";
        $statement = $em->prepare($prevfiscalSQL);
        $statement->execute();
        $PrevQuery = $statement->fetchAll();

        //Coalition SQL
        $coalition_id = '';
        foreach($PrevQuery as $item){
            if($item['coalition_id'] != 33){
                $coalition_id = $item['coalition_id'];
            }
        }
        $coalitionSQL=" SELECT coalition.coalition_name
                        FROM coalition
                        WHERE coalition.coalition_id = $coalition_id
                        LIMIT 1;";
        $statement = $em->prepare($coalitionSQL);
        $statement->execute();
        $coalitionName = $statement->fetchAll();

        //Current Fiscal Line Chart SQL
        $currentfiscalSQL=" SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, sample_data.month, sample_data.result, sample_data.data_thru
                            FROM sample_data
                            INNER JOIN item_numbers
                            ON sample_data.item_number = item_numbers.item
                            WHERE sample_data.coun_c = $county
                            AND item_numbers.category_id = 0
                            AND (sample_data.month > $years[1])
                            ORDER BY sample_data.month ASC;";
        $statement = $em->prepare($currentfiscalSQL);
        $statement->execute();
        $CurrentQuery = $statement->fetchAll();

        //Returns Fiscal Year Unix Time Stamps
        $date = (isset($CurrentQuery[0])) ? date('F j, Y', $CurrentQuery[0]['data_thru']) : date('F j, Y', $PrevQuery[0]['data_thru']);

        //Processes the previous and current fiscal year sql queries into presentable charts
        $LineResult = $this->linechart($PrevQuery, $CurrentQuery, $years);

        //Pie Chart SQL
        $pieSQL="   SELECT sample_data.item_number, item_numbers.item_description, item_numbers.category_id, category.description, sample_data.month, sample_data.result
                    FROM sample_data
                    INNER JOIN item_numbers
                    ON sample_data.item_number = item_numbers.item
                    INNER JOIN category
                    ON item_numbers.category_id = category.id
                    WHERE sample_data.coun_c = $county
                    AND item_numbers.category_id != 0
                    ORDER BY category.id ASC;";
        $statement = $em->prepare($pieSQL);
        $statement->execute();
        $PieQuery = $statement->fetchAll();

        //Process pie chart sql into presentable charts
        $PieResult = $this->piechart($PieQuery);

        //Combines the line and pie charts into the approriate arrays for display
        $charts = $this->chartsort($LineResult, $PieResult);

        //Return Data Arrays
        $json = array('title' => $coalitionName[0]['coalition_name'].' - '.$countyName[0]['coun_c_name'], 'county' => $countyName[0]['coun_c_name'], 'coalition' => $coalitionName[0]['coalition_name'], 'date' => $date, 'charts' => $charts, 'coalition_id' => $countyName[0]['coalition_id']);
        $response = new Response(json_encode($json));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /*
     * CountyDropdownAction() function
     *
     * Gets the list of coalitions to be displayed
     * in the coalition dropdown.
     */
    public function CountyDropdownAction()
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();
        $countydropdownSQL ="   SELECT DISTINCT sample_data.coalition_id, sample_data.coun_c, county.coun_c_name
                                FROM sample_data
                                INNER JOIN county
                                ON sample_data.coun_c = county.coun_c
                                INNER JOIN coalition
                                ON sample_data.coalition_id = coalition.coalition_id
                                WHERE sample_data.coun_c != 'ALL'
                                ORDER BY county.coun_c ASC;";
        $statement = $em->prepare($countydropdownSQL);
        $statement->execute();
        $results = $statement->fetchAll();
        $countycount = array();
        foreach($results as $item){
            if(
                isset($countycount[$item['coun_c']])
            ){
                $countycount[$item['coun_c']] += 1;
            }else{
                $countycount[$item['coun_c']] = 1;
            }
        }
        $item_array = array();
        //If county is in both a coalition and RMCA it will seperate it appropiately for the drop down.
        foreach($results as $item){
            if($countycount[$item['coun_c']] > 1 && $item['coalition_id'] == 33){
                $item_array[] = array('name' => $item['coun_c_name'].' - RCMA', 'id' => $item['coun_c'], 'link' => '#/county/coalition/'.$item['coun_c'].'/'.$item['coalition_id']);
            }elseif($countycount[$item['coun_c']] > 1 && $item['coalition_id'] != 33){
                $item_array[] = array('name' => $item['coun_c_name'], 'id' => $item['coun_c'], 'link' => '#/county/coalition/'.$item['coun_c'].'/'.$item['coalition_id']);
            }else{
                $item_array[] = array('name' => $item['coun_c_name'], 'id' => $item['coun_c'], 'link' => '#/county/'.$item['coun_c']);
            }
        }
        $response = new Response(json_encode($item_array));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /*
     * CoalitionDropdownAction() function
     *
     * Gets proper data for the coalition dropdown.
     */
    public function CoalitionDropdownAction()
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();
        $coalitiondropdownSQL ="    SELECT DISTINCT coalition.coalition_id, coalition.coalition_name, coalition_data.coalition_order
                                    FROM coalition
                                    INNER JOIN coalition_data
                                    ON coalition.coalition_id = coalition_data.coalition_id
                                    ORDER BY coalition_data.coalition_order ASC;";
        $statement = $em->prepare($coalitiondropdownSQL);
        $statement->execute();
        $results = $statement->fetchAll();
        $item_array = array();
        foreach($results as $item) {
            $item_array[] = array('name' => $item['coalition_name'], 'id' => $item['coalition_id'], 'link' => '#/coalition/'.$item['coalition_id']);
        }
        $response = new Response(json_encode($item_array));
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    /*
     * fiscalyears() function
     *
     * Based on current date will set time stamps
     * for previous and current fiscal year.
     */
    public function fiscalyears()
    {
        $fiscalstartdate = strtotime('Jul 1, '.(date('Y',time()) - 2));
        $fiscalenddate = strtotime('Jun 30, '.(date('Y',time()) - 1));
        return array($fiscalstartdate, $fiscalenddate);
    }

    /*
     * linechart() function
     *
     * Processes the previous and currnt fiscal year line charts. This
     * merges the values into a array for the same item_number and month
     * into the appropriate array for google charts. Additionally it creates
     * the proper descriptions and titles to be passed to javascript.
     */
    public function linechart(array $PrevQuery, array $CurrentQuery, array $years)
    {
        $item_array= array();
        //Processes the previous fiscal year query into charts
        foreach($PrevQuery as $item) {
            //Sets ItemNumber which is used to identify set of charts
            $ItemNumber = $item["item_number"];
            $ItemDescription = $item["item_description"];

            //Sets variables for each set of charts if not set
            if(
                !isset( $item_array[ $ItemNumber ] )
            ) {
                $item_array[ $ItemNumber ] = array(
                    "item_number" => $ItemNumber,
                    "item_description" => $ItemDescription,
                    "chart_title" => $ItemDescription,
                    "type" => "line",
                    "cols" => array(
                        array(
                            "id" => "",
                            "label" => "Month",
                            "type" => "string"
                        ),
                        array(
                            "id" => "",
                            "label" => "Previous Year",
                            "type" => "number"
                        ),
                    ),
                    "rows" => array()
                );
            }

            unset( $item["item_number"] );
            unset( $item["item_description"] );

            //Turns results into an array of rows for google charts
            $item_array[ $ItemNumber ]["rows"][] = array(
                'c' => array(
                    array(
                        "v" => date('M', $item['month']),
                    ),
                    array(
                        "v" => (float) $item['result'],
                    )
                )
            );
        }
        
        //Processes the current fiscal year query into charts
        foreach($CurrentQuery as $item) {
            //Sets ItemNumber which is used to identify set of charts
            $ItemNumber = $item["item_number"];
            $ItemDescription = $item["item_description"];

            //Sets variables for each set of charts if not set
            if(
                !isset( $item_array[ $ItemNumber ] )
            ) {
                $item_array[ $ItemNumber ] = array(
                    "item_number" => $ItemNumber,
                    "item_description" => $ItemDescription,
                    "chart_title" => $ItemDescription,
                    "type" => "line",
                    "cols" => array(
                        array(
                            "id" => "",
                            "label" => "Month",
                            "type" => "string"
                        ),
                        array(
                            "id" => "",
                            "label" => "Current Year",
                            "type" => "number"
                        ),
                    ),
                    "rows" => array()
                );
            }

            unset( $item["item_number"] );
            unset( $item["item_description"] );

            /*
             * Turns the columns to Previous and Current if there is corresponding data
             * for previous fiscal year. It then adds the value to the end of the row
             * array to the matching month. If there are no values in the row array
             * then it will create a new array.
             */
            $didntadd = true;
            foreach($item_array[ $ItemNumber ]["rows"] as $key => $row){
                if(substr($row['c'][0]['v'], 0,3) == date('M', $item['month']) ){
                    if(
                        !isset($item_array[ $ItemNumber ]['cols'][2])
                    ){
                        $item_array[ $ItemNumber ]['cols'] =array(
                                                                array(
                                                                    "id" => "Month",
                                                                    "label" => "Month",
                                                                    "type" => "string"
                                                                ),
                                                                array(
                                                                    "id" => "Previous",
                                                                    "label" => "Previous Year",
                                                                    "type" => "number"
                                                                ),
                                                                array(
                                                                    "id" => "Current",
                                                                    "label" => "Current Year",
                                                                    "type" => "number"
                                                                )
                                                            );
                    }
                    array_push($item_array[ $ItemNumber ]["rows"][$key]['c'], array('v' => (float) $item['result']));
                    $didntadd = false;
                }
            }
            //If no row data, create a new row
            if(
                $didntadd
            ){
                
                $item_array[ $ItemNumber ]["rows"][] = array(
                    'c' => array(
                        array(
                            "v" => date('M', $item['month']),
                        ),
                        array(
                            "v" => (float) $item['result'],
                        )
                    )
                );
            }
        }

        /*
         * To make sure data is passed correctly for google charts a null value is added to 
         * rows that only have one value.
         */
        foreach($item_array as $key => $item) {
            if(
                count($item['rows']) == 1
            ){
                $item_array[$key]['type'] = "summary";
            }
            if(
                count($item['cols']) == 3
            ){
                foreach($item['rows'] as $rowkey => $row){
                    if(
                        count($row['c']) < 3
                    ){
                        array_push($item_array[$key]['rows'][$rowkey]['c'], array('v' => null));
                    }
                }
            }
        }

        return $item_array;
    }

    public function piechart(array $results)
    {
        $item_array= array();
        //Processes the pie query into charts
        foreach($results as $item) {
            $ItemCatID = $item["category_id"];
            $CatDescription = $item["description"];

            if(
                !isset( $item_array[ $ItemCatID ] )
            ) {
                $item_array[ $ItemCatID ] = array(
                    "category_id" => $ItemCatID,
                    "category_description" => $CatDescription,
                    "chart_title" => $CatDescription,
                    "type" => "pie",
                    "cols" => array(
                        array(
                            "id" => "group",
                            "label" => "Group",
                            "type" => "string"
                        ),
                        array(
                            "id" => "result",
                            "label" => "Result",
                            "type" => "number"
                        ),
                    ),
                    "rows" => array()
                );
            }

            unset( $item["category_id"] );
            unset( $item["description"] );

            //Turns results into an array of rows for google charts
            $item_array[ $ItemCatID ]["rows"][] = array(
                'c' => array(
                    array(
                        "v" => $item['item_description'],
                    ),
                    array(
                        "v" => (float) $item['result'],
                    )
                )
            );
        }
        return $item_array;
    }

    /*
     * chartsort() function
     * 
     * Sorts all the charts into the groupings designated
     * by OEL for display.
     */
    public function chartsort(array $line, array $pie)
    {
        //Establishes connection to database
        $doctrineManager= $this->container->get('doctrine')->getManager();
        $em = $doctrineManager->getConnection();

        $item_array = array();

        //Pulls chart mapping table to pass correct title and chart assocations
        $chartSQL=" SELECT *
                    FROM chart_mapping;";
        $statement = $em->prepare($chartSQL);
        $statement->execute();
        $ChartQuery = $statement->fetchAll();
        //Loops through each chart_mapping item
        foreach(
            $ChartQuery as $key => $ChartItem
        ){
            //Loops through all the line charts
            foreach(
                $line as $linekey => $item
            ){
                //If line chart total is in mapping table row then add it to to chart object
                if ($ChartItem['total_code'] == $linekey) {
                    if (substr($linekey, -6) == '-TOTAL' || substr($linekey, -8) == '-DOLLARS') {
                        $item_array['chart-'.$ChartItem['chart_id']]['summary_total'] = $item['rows'][0]['c'][1]['v'];
                        unset($line[$linekey]);
                    } else {
                        $Cmap = 1;
                        //find whether current year is C1 or C2
                        foreach ($item['cols'] as $thiscolkey => $thiscol) {
                            if ($thiscol['label']=='Current Year') {
                                $Cmap = $thiscolkey;
                                break;
                            }
                        }
                        $loopme = array_reverse($item['rows']);
                        foreach($loopme as $rowkey => $row){
                            if(isset($row['c'][$Cmap]) && $row['c'][$Cmap]['v'] != null){
                                $item_array['chart-'.$ChartItem['chart_id']]['summary_total'] = $loopme[$rowkey]['c'][$Cmap]['v'];
                                unset($line[$linekey]);
                                break;
                            }
                        }
                    }
                }
                //If line chart value is in mapping table row
                if(
                    $ChartItem['pie_chart_code'] == $linekey || $ChartItem['line_chart_code'] == $linekey
                ){
                    //If chart_title, description, total, or tabs are not set then run this to set values
                    if(
                        !isset($item_array['chart-'.$ChartItem['chart_id']]['chart_title']) || !isset($item_array['chart-'.$ChartItem['chart_id']]['description']) || !isset($item_array['chart-'.$ChartItem['chart_id']]['total']) || !isset($item_array['chart-'.$ChartItem['chart_id']]['tabs'])
                    ){
                        $item_array['chart-'.$ChartItem['chart_id']]['chart_title'] = $ChartItem['summary_indicator'];
                        $item_array['chart-'.$ChartItem['chart_id']]['description'] = $ChartItem['description'];
                        //If tab names aren't set then set them
                        if(
                            $ChartItem['first_tab'] != "" || $ChartItem['second_tab'] != ""
                        ){
                            $item_array['chart-'.$ChartItem['chart_id']]['tabs'] = array($ChartItem['first_tab'], $ChartItem['second_tab']);
                            if($ChartItem['third_tab'] != null){ $item_array['chart-'.$ChartItem['chart_id']]['tabs'][] = $ChartItem['third_tab']; }
                        }
                    }
                    $item_array['chart-'.$ChartItem['chart_id']][$linekey] = $item;
                    unset($line[$linekey]);
                }
                //Currently these line charts are never to be displayed despite being in the data they give us
                if(
                    $linekey == 'CL-TOTAL' || $linekey == 'PG-TOTAL'
                ){
                    unset($line[$linekey]);
                }
            }
            //Loops through all the pie charts
            foreach(
                $pie as $piekey => $item
            ){
                //If pie chart value is in mapping table row
                if(
                    $ChartItem['pie_chart_code'] == $piekey || $ChartItem['line_chart_code'] == $piekey || $ChartItem['second_pie_chart_code'] == $piekey
                ){
                    //If chart_title, description, total, or tabs are not set then run this to set values
                    if(
                        !isset($item_array['chart-'.$ChartItem['chart_id']]['chart_title']) || !isset($item_array['chart-'.$ChartItem['chart_id']]['description']) || !isset($item_array['chart-'.$ChartItem['chart_id']]['tabs'])
                    ){
                        $item_array['chart-'.$ChartItem['chart_id']]['chart_title'] = $ChartItem['summary_indicator'];
                        $item_array['chart-'.$ChartItem['chart_id']]['description'] = $ChartItem['description'];
                        //If tab names aren't set then set them
                        if(
                            $ChartItem['first_tab'] != "" || $ChartItem['second_tab'] != ""
                        ){
                            $item_array['chart-'.$ChartItem['chart_id']]['tabs'] = array($ChartItem['first_tab'], $ChartItem['second_tab']);
                            if($ChartItem['third_tab'] != null){ $item_array['chart-'.$ChartItem['chart_id']]['tabs'][] = $ChartItem['third_tab']; }
                        }
                    }
                    $item_array['chart-'.$ChartItem['chart_id']][$piekey] = $item;
                    unset($pie[$piekey]);
                }
            }
        }
        //Merge item_array with remaining line charts
        $result = array_merge($item_array, $line);

        //Merge item_array with remaining pie charts
        $result = array_merge($result, $pie);
        return $result;
    }

}